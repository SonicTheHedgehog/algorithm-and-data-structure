#pragma once
#include <string>
using namespace std;
class Queue
{
  string *lines;
  size_t _size_;

public:
  Queue(size_t n);
  ~Queue();
  string get(int index);
  void set(int index, string instring);
  size_t size();
  void isEmpty();
  void print();
  void enqueue(string value);
  void dequeue();
  void clear();
};