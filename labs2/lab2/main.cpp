#include "queue.h"
#include <iostream>
using namespace std; 
int main()
{
    size_t n = 0;
    string line;
    cout << "Enter the size of array" << endl;
    cin >> n;
    Queue Q(n);
    while (1)
    {
        cout << "\nEnter the string" << endl;
        cin >> line;
        if (line.length() == 2 || line.length() == 6)
        {
            if (line.length() == 2 && isalpha(line[0]) && isalpha(line[1]))
            {
                Q.dequeue();
                Q.print();
            }
            else if (line.length() == 6 && isdigit(line[0]) && isdigit(line[1]) && isdigit(line[3]) && isdigit(line[4]) && isalpha(line[2]) && isalpha(line[5]))
            {
                Q.enqueue(line);
                Q.print();
            }
            else
            {
                cerr << "Input is incorrect. Try again." << endl;
            }
        }
        else
        {
            cerr << "Enter just string of 6 or 2 symbols" << endl;
        }
    }
    return 0;
}