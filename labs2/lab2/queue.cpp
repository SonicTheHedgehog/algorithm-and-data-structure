#include "queue.h"
#include <iostream>
static int count = 0;
Queue::Queue(size_t n)
{
    _size_ = n;
    lines = new string[_size_];
}
size_t Queue::size()
{
    return _size_;
}
void Queue::enqueue(string value)
{
    if (count == _size_)
    {
        std::cout << "The queue is full." << std::endl;
        this->print();
        this->clear();
        exit(1);
    }
    else
    {
        this->set(count, value);
        count++;
    }
}
void Queue::clear()
{
    while (_size_ != 0)
    {
        for (int i = 0; i < count-1 ; i++)
        {
            this->set(i, this->get(i + 1));
        }
        count--;
        _size_--;
        this->print();
    }
    this->isEmpty();
}
void Queue::isEmpty()
{
    if (_size_ == 0|| count==0)
    {
        std::cout << "The queue is empty." << std::endl;
        exit(1);
    }
}
void Queue::dequeue()
{
    this->isEmpty();
    for (int i = 0; i < count; i++)
    {
        this->set(i, this->get(i + 1));
    }
    count--;
}
void Queue::print()
{
    this->isEmpty();
    for (int i = 0; i < count; i++)
    {
        std::cout << this->get(i) << " ";
    }
    cout << " " << endl;
}
string Queue::get(int index)
{
    if (index < 0 || index >= _size_)
        std::cerr << "there is no such index" << std::endl;
    else
        return lines[index];
}
void Queue::set(int index, string instring)
{
    if (index < 0 || index >= _size_)
        std::cerr << "there is no such index" << std::endl;
    else
        lines[index] = instring;
}
Queue::~Queue()
{
    delete[] lines;
}