#include "operations.h"
string LogicalNo(string value)
{
    if (value == "T")
    {
        return "F";
    }
    else if (value == "F")
    {
        return "T";
    }
    else
    {
        cerr << "Error with logical no " << endl;
        exit(1);
    }
}
string LogicalOr(string one, string anth)
{
    if (one == "T" || anth == "T")
    {
        return "T";
    }
    else if (one == "F" || anth == "F")
    {
        return "F";
    }
    else
    {
        cerr << "Error with logical or" << endl;
        exit(1);
    }
}
string LogicalAnd(string one, string anth)
{
    if (one == "T" && anth == "T")
    {
        return "T";
    }
    else if (one == "F" || anth == "F")
    {
        return "F";
    }
    else
    {
        cerr << "Error with logical and" << endl;
        exit(1);
    }
}
string Answer(string one, string anth, string operat)
{
    if (operat == "||")
    {
        return LogicalOr(one, anth);
    }
    else if (operat == "&&")
    {
        return LogicalAnd(one, anth);
    }
    else if (operat == "!")
    {
        if (one.size() != 0)
        {
            return LogicalNo(one);
        }
        else if (anth.size() != 0)
        {
            return LogicalNo(anth);
        }
        else
        {
            cerr << "Error with operand !" << endl;
            exit(1);
        }
    }
    else
    {
        cerr << "No right answer" << endl;
        exit(1);
    }
}