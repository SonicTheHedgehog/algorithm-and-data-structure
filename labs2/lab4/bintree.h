#pragma once
#include <string>
#include <stack>
#include <iostream>
#include "operations.h"
using namespace std;
struct Node
{
   string data = "";
   Node *left = nullptr;
   Node *right = nullptr;
   Node *parent = nullptr;
};

class BinTree
{
   Node *root = nullptr;
   Node *this_Node;
   int size = 0;

public:
   BinTree();
   int sizeTree();
   void insert(string value);
   void deleteTree();
   void print();
   bool result();
};