#include "bintree.h"
#include "operations.h"
#include <vector>
#include <string>
using namespace std;
Node *newNode(Node *root)
{
  if (root == nullptr)
  {
    root->data = "";
    root->left = nullptr;
    root->right = nullptr;
    root->parent = nullptr;
    return root;
  }
  else
  {
    root->left = nullptr;
    root->right = nullptr;
    return root;
  }
  return root;
}
int BinTree::sizeTree()
{
  return this->size;
}

BinTree::BinTree()
{
  if (root == nullptr)
  {
    Node *tmp = new Node;
    root = newNode(tmp);
    this_Node = root;
  }
}

void BinTree::insert(string data_)
{
  if (data_ == "(")
  {
    Node *tmp = new Node;
    this_Node->left = newNode(tmp);
    this_Node->left->parent = this_Node;
    this_Node = this_Node->left;
  }
  else if (data_ == "&&" || data_ == "||")
  {
    this_Node->data = data_;
    Node *tmp = new Node;
    this_Node->right = newNode(tmp);
    this_Node->right->parent = this_Node;
    this_Node = this_Node->right;
  }
  else if (data_ == "!")
  {
    this_Node->parent->data = data_;
  }
  else if (data_ == "T" || data_ == "F")
  {
    this_Node->data = data_;
    this_Node = this_Node->parent;
  }
  else if (data_ == ")")
  {
    this_Node = this_Node->parent;
  }
}

void clear(Node *&tree)
{
  if (tree->left)
  {
    clear(tree->left);
  }
  if (tree->right)
  {
    clear(tree->right);
  }
  delete tree;
}

void BinTree::deleteTree()
{
  clear(this->root);
}

void printValueOnLevel(Node *node, char pos, int depth)
{
  for (int i = 0; i < depth; i++)
  {
    cout<<("....");
  }
  printf("%c: ", pos);

  if (node == nullptr)
  {
    cout<<"NULL\n";
  }
  else
  {
    cout << node->data << endl;
  }
}

void printNode(Node *node, char pos, int depth)
{
  bool hasChild = node != nullptr && (node->left != nullptr || node->right != nullptr);
  if (hasChild)
    printNode(node->right, 'R', depth + 1);
  printValueOnLevel(node, pos, depth);
  if (hasChild)
    printNode(node->left, 'L', depth + 1);
}

void printBinTree(Node *root)
{
  printNode(root,'+', 0);
}

void BinTree::print()
{
  printBinTree(root);
}

string evaluate(Node *root)
{
  Node *left_ = root->left;
  Node *right_ = root->right;
  if (left_ != nullptr && right_ != nullptr)
  {
    return Answer(evaluate(left_), evaluate(right_), root->data);
  }
  else if (left_ != nullptr)
  {
    return Answer(evaluate(left_), "", root->data);
  }
  else
  {
    return root->data;
  }
}

bool BinTree::result()
{
  string res = evaluate(this->root);
  if (res == "T")
  {
    return true;
  }
  else if (res == "F")
  {
    return false;
  }
  else
  {
    cerr << "Incorrect input" << endl;
    exit(1);
  }
}