#include "bintree.h"
#include "parseInput.h"
using namespace std;
bool checkinput(vector<string> text);
void mainFunction(vector<string> array);
int main()
{
  cout << "Example: ((T&&F)||(!F))" << endl;
  vector<string> sm = parce("((T&&F)||(!F))");
  mainFunction(sm);
  cout<<"Now your tern"<<endl;
  while (1)
  {
    cout << "Enter the expression or 1 for exit " << endl;
    string tmp;
    cin >> tmp;
    if (tmp == "1")
    {
      exit(1);
    }
    else
    {
      vector<string> array = parce(tmp);
      if (checkinput(array))
      {
        mainFunction(array);
      }
      else
      {
        cerr << "Incorrect expression" << endl;
        exit(1);
      }
    }
  }
}
void mainFunction(vector<string> array)
{
  cout << endl;
  BinTree Tree;
  for (int i = 0; i < array.size(); i++)
  {
    Tree.insert(array[i]);
  }
  Tree.print();
  cout << endl;
  cout << "Result: ";
  bool res = Tree.result();
  if (res)
  {
    cout << "TRUE" << endl;
  }
  else
  {
    cout << "FALSE" << endl;
  }
  Tree.deleteTree();
}
bool checkinput(vector<string> text)
{
  int colOper = 0;
  int colPar = 0;
  stack<int> tmp;
  for (int i = 0; i < text.size(); i++)
  {
    if (text[i][0] == '&' || text[i][0] == '|' || text[i][0] == '!')
    {
      colOper++;
    }
    if (text[i][0] == '(')
    {
      colPar++;
      tmp.push(1);
    }
    if (text[i][0] == ')')
    {
      tmp.pop();
    }
  }

  if (!tmp.empty())
  {
    return false;
  }
  if (colOper != colPar)
  {
    return false;
  }

  return true;
}