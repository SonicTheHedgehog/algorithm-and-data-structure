#include "parseInput.h"

vector<string> parce(string in)
{
    vector<string> result;
    int k = 0;
    for (int i = 0; in[i] != '\0'; i++)
    {
        if (in[i] == '&')
        {
            if (i != 0)
            {
                if (in[i - 1] == '&')
                {
                    result.push_back("");
                    result[k].push_back(in[i - 1]);
                    result[k].push_back(in[i]);
                    k++;
                }
            }
            else
            {
                cout << "Error parsing. Wrong expression.";
                exit(1);
            }
        }
        else if (in[i] == '|')
        {
            if (i != 0)
            {
                if (in[i - 1] == '|')
                {
                    result.push_back("");
                    result[k].push_back(in[i - 1]);
                    result[k].push_back(in[i]);
                    k++;
                }
            }
            else
            {
                cout << "Error parsing. Wrong expression.";
                exit(1);
            }
        }
        else if (in[i] == ' ')
        {
        }
        else
        {
            result.push_back("");
            result[k].push_back(in[i]);
            k++;
        }
    }

    int sizeArray = 0;
    for (int i = 0; i < result.size(); i++)
    {
        sizeArray += result[i].size();
    }

    if (sizeArray != in.size())
    {
        cerr << "Error incorrect expression." << endl;
        exit(1);
    }

    return result;
}