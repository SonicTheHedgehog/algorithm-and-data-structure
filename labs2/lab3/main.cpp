#include "hashtable2.h"
#include "hashtable.h"
using namespace std;
bool isNumber(string str)
{
    if (str.empty())
        return false;
    for (int i = 0; i < str.length(); i++)
    {
        if (!isdigit(str.at(i)))
            return false;
    }
    return true;
}
int main()
{
    Hashtable tab;
    Hashtable2 tab2;
    tab.inithashtable(5);
    tab2.inithashtable2(10);
    Key key = {10000};
    Date d1 = {2020, "July", 5};
    Value v1 = {"Adam", 1, "for one", d1};
    tab.insertValue(key, v1);
    tab2.insertName(d1, v1.mainGuestName);
    while (1)
    {
        cout << "Choose an option:\n1. Print table\n2. Add new guest\n3. Find guest by key\n4. Remove guest\n5. Print table with dates\n6. Arrivals at this data\n7.  Exit " << endl;
        string choise;
        cin >> choise;
        if (isNumber(choise))
        {
            if (stoi(choise) == 1)
            {
                system("clear");
                tab.printTable();
            }
            else if (stoi(choise) == 2)
            {
                cout << "New guest information:" << endl;
                string name, guests, room, day, month, year;
                cout << "Name: " << endl;
                cin.ignore();
                getline(cin, name);
                cout << "Number of guests:\n";
                cin >> guests;
                cout << "Type of room:\n";
                cin.ignore();
                cin >> room;
                cout << "Date of arrival:\nday:\n";
                cin >> day;
                cout << "month:\n";
                cin.ignore();
                cin >> month;
                cout << "year:\n";
                cin >> year;
                if (isNumber(guests) && isNumber(day) && isNumber(year))
                {
                    int g = stoi(guests);
                    int d = stoi(day);
                    int y = stoi(year);
                    Date date = {y, month, d};
                    Value value = {name, g, room, date};
                    key = {key.bookingCode + 3};
                    tab.insertValue(key, value);
                    tab2.insertName(date, value.mainGuestName);
                    system("clear");
                    cout << "New guest added!" << endl;
                }
                else
                {
                    cout << "Incorrect input of number of guests or data of arrival. Try again." << endl;
                }
            }
            else if (stoi(choise) == 3)
            {
                string k;
                cout << "Enter the key of guest:\n";
                cin >> k;
                system("clear");
                if (isNumber(k))
                {
                    Key kk = {stoi(k)};
                    tab.printEntry(kk);
                }
                else
                {
                    cout << "Incorrect input of key." << endl;
                }
            }
            else if (stoi(choise) == 4)
            {
                string k;
                cout << "Enter the key of guest:\n";
                cin >> k;
                system("clear");
                if (isNumber(k))
                {
                    Key kk = {stoi(k)};
                    Guest guest = tab.getGuest(kk);
                    system("clear");
                    bool t = tab.removeValue(kk);
                    bool t2 = tab2.removeName(guest.value.dateOfArrival, guest.value.mainGuestName);
                    if (t && t2)
                    {
                        tab.printTable();
                    }
                    else
                    {
                        cerr << "Not removed." << endl;
                    }
                }
            }
            else if (stoi(choise) == 5)
            {
                system("clear");
                tab2.printTable2();
            }
            else if (stoi(choise) == 6)
            {
                string day, month, year;
                cout << "Enter the date of arrival:\nday:\n";
                cin >> day;
                cout << "month:\n";
                cin.ignore();
                cin >> month;
                cout << "year:\n";
                cin >> year;
                if (isNumber(day) && isNumber(year))
                {
                    system("clear");
                    int d = stoi(day);
                    int y = stoi(year);
                    tab2.allArrivalsAtThisDate({y, month, d});
                }
                else
                {
                    cout << "Incorrect input of number of data of arrival. Try again." << endl;
                }
            }
            else if (stoi(choise) == 7)
            {
                system("clear");
                exit(1);
            }
            else
            {
                cerr << "Wrong choise. Choose from 1 to 7" << endl;
            }
        }
        else
        {
            cerr << "Wrong input. Try input just numbers." << endl;
        }
    }
    return 0;
}