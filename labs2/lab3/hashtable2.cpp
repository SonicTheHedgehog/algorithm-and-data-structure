#include "hashtable2.h"
using namespace std;
Hashtable2::Hashtable2(size_t size)
{
    vector<Arrivals> table{size};
    capacity = size;
    loadness = 0;
}
void Hashtable2::inithashtable2(size_t size)
{
    Hashtable2 newtab{size};
    for (int i = 0; i < newtab.capacity; i++)
    {
        Date d = {0, "", 0};
        Arrivals ar = createArry(d, "");
        newtab.table.push_back(ar);
    }
    this->table = newtab.table;
    this->capacity = newtab.capacity;
    this->loadness = newtab.loadness;
}
Arrivals Hashtable2::createArry(Date key, string name)
{
    Arrivals ar;
    ar.key = key;
    ar.guestname.push_back(name);
    ar.deleted.isDeleted = false;
    return ar;
}
void Hashtable2::insertName(Date key, string name)
{
    Arrivals a = createArry(key, name);
    double loadfactor = (double)this->loadness / (double)this->capacity;
    if (loadfactor > 0.5)
    {
        cout << "Rehashing has been done" << endl;
        this->rehashing();
    }
    int hash = getHash(key);
    if (this->table[hash].key.day == key.day && this->table[hash].key.month == key.month && this->table[hash].key.year == key.year)
    {
        this->table[hash].guestname.push_back(name);
    }
    else
    {
        this->table[hash] = a;
        this->loadness++;
    }
}

int monthHash(string key_month)
{

    if (key_month == "January")
    {
        return 1;
    }
    else if (key_month == "February")
    {
        return 2;
    }
    else if (key_month == "March")
    {
        return 3;
    }
    else if (key_month == "April")
    {
        return 4;
    }
    else if (key_month == "May")
    {
        return 5;
    }
    else if (key_month == "June")
    {
        return 6;
    }
    else if (key_month == "July")
    {
        return 7;
    }
    else if (key_month == "August")
    {
        return 8;
    }
    else if (key_month == "September")
    {
        return 9;
    }
    else if (key_month == "October")
    {
        return 10;
    }
    else if (key_month == "November")
    {
        return 11;
    }
    else if (key_month == "December")
    {
        return 12;
    }
}
int Hashtable2::hashCode(Date key)
{
    int day = key.day;
    int year = key.year;
    int month = monthHash(key.month);

    return 1000000 * day + 10000 * month + year;
}

int Hashtable2::getHash(Date key)
{
    int i = 0;
    int h = 0;
    bool v = true;
    int hash = hashCode(key);
    while (v)
    {
        h = hash + pow(i, 2);
        h %= this->capacity;
        if (this->table[h].key.day == key.day && this->table[h].key.month == key.month && this->table[h].key.year == key.year)
        {
            v = false;
        }
        if (this->table[h].key.day == 0 || this->table[h].deleted.isDeleted)
        {
            v = false;
        }
        else
            i++;
    }
    return h;
}
void Hashtable2::rehashing()
{
    Hashtable2 tab;
    int tmp = this->capacity;
    tab.inithashtable2(tmp);
    tab.loadness = this->loadness;
    for (int i = 0; i < tmp; i++)
    {
        tab.table.push_back(this->table[i]);
        tab.table[i].deleted = this->table[i].deleted;
        tab.table[i].key = this->table[i].key;
        for (auto item = this->table[i].guestname.begin(); item != this->table[i].guestname.end(); ++item)
        {
            tab.table[i].guestname.push_back(*item);
        }
    }
    this->inithashtable2(tmp * 2);
    this->loadness = tab.loadness;
    for (int i = 0; i < tmp; i++)
    {
        this->table.push_back(tab.table[i]);
        this->table[i].deleted = tab.table[i].deleted;
        this->table[i].key = tab.table[i].key;
        for (auto item = tab.table[i].guestname.begin(); item != tab.table[i].guestname.end(); ++item)
        {
            this->table[i].guestname.push_back(*item);
        }
    }
}

void showList(list<string> list_table)
{
    for (auto item = list_table.begin(); item != list_table.end(); ++item)
    {
        cout << *item;
        if (*item != "")
        {
            cout << ",";
        }
    }
    cout << endl;
}
void Hashtable2::printTable2()
{
    cout << "----------------Table of Arrivals------------------" << endl;
    for (int i = 0; i < this->capacity; i++)
    {
        if (this->table[i].deleted.isDeleted)
        {
            cout << 0 << " | Deleted" << endl;
        }
        else if (this->table[i].key.day != 0)
        {
            cout << "\nDate of arrival: " << this->table[i].key.day << " " << this->table[i].key.month << " " << this->table[i].key.year << " | ";
            cout << "\tName: ";
            showList(this->table[i].guestname);
        }
    }
    cout << "\n------------------------------------------" << endl;
}
int Hashtable2::findValue(Date key)
{
    int i = 0;
    int h = 0;
    bool v = true;
    int hash = hashCode(key);
    while (v)
    {

        h = hash + pow(i, 2);
        h %= this->capacity;
        if (this->table[h].key.day==0)
        {
            return -1;
        }
        else
        {
            if (this->table[h].key.day == key.day && this->table[h].key.month == key.month && this->table[h].key.year == key.year)
            {
                v = false;
            }
            else
                i++;
        }
    }
    return h;
}
bool Hashtable2::removeName(Date k, string name)
{
    int size, count = 0;
    int hash = findValue(k);
    if (hash != -1)
    {
        size = this->table[hash].guestname.size();
        if (size <= 1)
        {
            Date d = {0, "", 0};
            Arrivals g;
            g = createArry(d, "");
            g.deleted.isDeleted = true;
            this->table[hash] = g;
        }
        else
        {
            for (list<string>::iterator item = this->table[hash].guestname.begin(); item != this->table[hash].guestname.end(); item++)
            {
                if (*item == name)
                {
                    table[hash].guestname.erase(item);
                    return true;
                }
            }
            if (count == 0)
            {

                cout << "There is no such guest on this date" << endl;
                return false;
            }
        }
        return true;
    }
    else
    {
        return false;
    };
}
void Hashtable2::allArrivalsAtThisDate(Date dateOfArrival)
{
    int hash = findValue(dateOfArrival);
    if (hash != -1)
    {
        cout << "Guests on the day: " << this->table[hash].key.day << " " << this->table[hash].key.month << " " << this->table[hash].key.year << endl;
        showList(this->table[hash].guestname);
        cout << " " << endl;
    }
    else
    {
        cout << "There is no such key" << endl;
    };
}