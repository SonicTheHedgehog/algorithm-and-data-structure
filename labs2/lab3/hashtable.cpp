#include "hashtable.h"
using namespace std;
Hashtable::Hashtable(size_t size)
{
    vector<Guest> table{size};
    capacity = size;
    loadness = 0;
}
void Hashtable::inithashtable(size_t size)
{
    Hashtable newtab{size};
    for (int i = 0; i < newtab.capacity; i++)
    {
        Date d = {0, "", 0};
        Value v = {"", 0, "", d};
        Guest g = createEntry({0}, v);
        table.push_back(g);
    }
    this->table = newtab.table;
    this->capacity = newtab.capacity;
    this->loadness = newtab.loadness;
}
Guest Hashtable::createEntry(Key key, Value &value)
{
    Guest entry;
    entry.key = key;
    entry.value = value;
    entry.deleted.isDeleted = false;
    return entry;
}
void Hashtable::insertValue(Key k, Value &v)
{
    Guest g = createEntry(k, v);
    double loadfactor = (double)this->loadness / (double)this->capacity;
    if (loadfactor > 0.5)
    {
        cout << "Rehashing has been done" << endl;
        this->rehashing();
    }
    int hash = getHash(k);
    this->table[hash] = g;
    this->loadness++;
}
int Hashtable::findValue(Key key)
{
    int i = 0;
    int h = 0;
    bool v = true;
    int hash = hashCode(key);
    while (v)
    {
        h = hash + pow(i, 2);
        h = h % this->capacity;
        if(this->table[h].key.bookingCode==0)
        {
            return -1;
        }
        else
        {

            if (this->table[h].key.bookingCode == key.bookingCode)
            {
                v = false;
            }
            else
                i++;
        }
    }
    return h;
}
bool Hashtable::removeValue(Key k)
{
    int hash = findValue(k);
    if (hash != -1)
    {
        Date d = {0, "", 0};
        Value v = {"", 0, "", d};
        Guest g = createEntry({0}, v);
        g.deleted.isDeleted = true;
        this->table[hash] = g;
        return true;
    }
    else
    {
        cout << "There is no such key" << endl;
        return false;
    }
}
int Hashtable::hashCode(Key key)
{
    return (key.bookingCode - 10000);
}
int Hashtable::getHash(Key key)
{
    int i = 0;
    int h = -1;
    bool v = true;
    int hash = hashCode(key);
    while (v)
    {
        h = hash + pow(i, 2);
        h = h % this->capacity;
        if (this->table[h].key.bookingCode == 0 || this->table[h].deleted.isDeleted)
        {
            v = false;
        }
        else
            i++;
    }
    return h;
}
void Hashtable::rehashing()
{
    Hashtable tab;
    int tmp = this->capacity;
    tab.inithashtable(tmp);
    tab.loadness = this->loadness;
    for (int i = 0; i < tmp; i++)
    {
        tab.table.push_back(this->table[i]);
        tab.table[i].deleted = this->table[i].deleted;
    }
    this->inithashtable(tmp * 2);
    this->loadness = tab.loadness;
    for (int i = 0; i < tmp; i++)
    {
        this->table.push_back(tab.table[i]);
        this->table[i].deleted = tab.table[i].deleted;
    }
}
void Hashtable::printEntry(Key k)
{
    int hash = findValue(k);
    if (hash != -1)
    {
        cout << "----------------Guest------------------" << endl;
        cout << this->table[hash].key.bookingCode << " | ";
        if (this->table[hash].deleted.isDeleted)
        {
            cout << "Deleted" << endl;
        }
        else if (this->table[hash].key.bookingCode == 0)
        {
            cout << "_________" << endl;
        }
        else
        {
            cout << "Name: " << this->table[hash].value.mainGuestName << "\n\tGuests: " << this->table[hash].value.numberOfGuests << endl;
            cout << "\tDate of arrival: " << this->table[hash].value.dateOfArrival.day << " " << this->table[hash].value.dateOfArrival.month << " " << this->table[hash].value.dateOfArrival.year;
        }
        cout << "\n------------------------------------------" << endl;
    }
    else
    {
        cerr << "There is no such key." << endl;
    }
}
void Hashtable::printTable()
{
    cout << "----------------Main Table------------------" << endl;
    for (int i = 0; i < this->capacity; i++)
    {
        cout << this->table[i].key.bookingCode << " | ";
        if (this->table[i].key.bookingCode != 0)
        {
            cout << "Name: " << this->table[i].value.mainGuestName << "\n\tGuests: " << this->table[i].value.numberOfGuests << endl;
            cout << "\tDate of arrival: " << this->table[i].value.dateOfArrival.day << " " << this->table[i].value.dateOfArrival.month << " " << this->table[i].value.dateOfArrival.year << endl;
        }
        else if (this->table[i].deleted.isDeleted)
        {
            cout << "Deleted" << endl;
        }
        else if (this->table[i].key.bookingCode == 0)
        {
            cout << "_________" << endl;
        }
    }
    cout << "\n---------------------------------------" << endl;
}
Guest Hashtable::getGuest(Key k)
{
    int hash = findValue(k);
    Guest arr;
    arr.value.mainGuestName = this->table[hash].value.mainGuestName;
    arr.value.numberOfGuests = this->table[hash].value.numberOfGuests;
    arr.key.bookingCode = this->table[hash].key.bookingCode;
    arr.value.dateOfArrival.day = this->table[hash].value.dateOfArrival.day;
    arr.value.dateOfArrival.month = this->table[hash].value.dateOfArrival.month;
    arr.value.dateOfArrival.year = this->table[hash].value.dateOfArrival.year;
    arr.deleted.isDeleted = false;
    return arr;
}