#pragma once
#include "hashtable.h"
#include <list>
using namespace std;
struct Arrivals
{
    Date key;
    list<string> guestname;
    Deleted deleted;
};
class Hashtable2
{
    vector<Arrivals> table;
    int loadness;
    int capacity;

    int hashCode(Date key);
    int getHash(Date key);
    void rehashing();
    Arrivals createArry(Date key, string name);
    int findValue(Date key);

public:
    explicit Hashtable2(size_t size = 0);
    ~Hashtable2() {}

    void inithashtable2(size_t size);
    void insertName(Date key, string name);
    bool removeName(Date key,string name );
    void printTable2();
    void allArrivalsAtThisDate(Date dateOfArrival);
};
