#pragma once
#include <string>
#include <vector>
#include <iostream>
#include <math.h>
using namespace std;

struct Key
{
    int bookingCode;
};
struct Date
{
    int year;
    string month;
    int day;
};
struct Value
{
    string mainGuestName;
    int numberOfGuests;
    string roomType;
    Date dateOfArrival;
};
struct Deleted
{
    bool isDeleted;
};
struct Guest
{
    Key key;
    Value value;
    Deleted deleted;
};
class Hashtable
{
    vector<Guest> table;
    int loadness;
    int capacity;

    int hashCode(Key key);
    int getHash(Key key);
    void rehashing();
    int findValue(Key key);
    Guest createEntry(Key key, Value &value);

public:
    explicit Hashtable(size_t size = 0);
    ~Hashtable() {}

    void inithashtable(size_t size);
    void insertValue(Key k, Value &v);
    bool removeValue(Key k);
    Guest getGuest(Key k);
    void printTable();
    void printEntry(Key k);
};
