#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <math.h>
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define RESET "\x1B[0m"
int mostSignif(int a)
{
    int b = 1;
    while (a / b > 0)
        b *= 10;
    return b/10;
}
void radixsort1(int size, int max, int *arr)
{
    int number = mostSignif(max);
    for (int i = number; i > 0.1; i /= 10)
    {
        for (int j = 0; j < size; j++)
        {
            for (int c = 0; c < size; c++)
            {
                if (arr[j] / i < arr[c] / i)
                {
                    int a = 0;
                    a = arr[j];
                    arr[j] = arr[c];
                    arr[c] = a;
                }
            }
        }
    }
}
void radixsort2(int size, int max, int *arr)
{
    int number = mostSignif(max);
    for (int i = number; i > 0.1; i /= 10)
    {
        for (int j = 0; j < size; j++)
        {
            for (int c = 0; c < size; c++)
            {
                if (arr[j] / i > arr[c] / i)
                {
                    int a = 0;
                    a = arr[j];
                    arr[j] = arr[c];
                    arr[c] = a;
                }
            }
        }
    }
}
int threetoten(int number)
{
    int a = 0, k = 0, z = 0;
    for (int i = number; i > 0.1; i /= 10)
    {
        if (i % 10 >= 3)
        {
            printf(KBLU "Wrong input\n" RESET);
            exit(1);
        }
        else
        {
            z = i % 10;
            a += z * pow(3, k);
            k++;
        }
    }
    return a;
}
int main()
{
    printf(KYEL "Here is an example of program:\n" RESET);
    int example[10] = {212, 101, 2, 1, 21120, 222, 21, 12, 10210, 2};
    int answer[10] = {21120, 10210, 1, 2, 222, 212, 101, 2, 21, 12};
    for (int i = 0; i < 10; i++)
    {
        printf("%i ", example[i]);
    }
    puts("");
    for (int i = 0; i < 10; i++)
    {
        printf("%i ", answer[i]);
    }
    int n = 0;
    printf(KYEL "\nEnter the number of numbers:\n" RESET);
    scanf("%i", &n);
    int arrmain[n];
    printf(KYEL "Enter numbers for sorting:\n" RESET);
    for (int i = 0; i < n; i++)
    {
        scanf("%i", &arrmain[i]);
        threetoten(arrmain[i]);
        if (arrmain[i] == 0)
        {
            printf(KBLU "Wrong input\n" RESET);
            exit(1);
        }
    }
    int max1 = 0, max2 = 0;
    int index1[n], index2[n], newarr1[n], newarr2[n], y = 0, z = 0;
    for (int i = 0; i < n; i++)
    {
        if (threetoten(arrmain[i]) > n / 2)
        {
            index1[z] = i;
            newarr1[z] = arrmain[i];
            if (arrmain[i] > max1)
            {
                max1 = arrmain[i];
            }
            printf(KGRN "%i " RESET, arrmain[i]);
            z++;
        }
        else
        {
            index2[y] = i;
            newarr2[y] = arrmain[i];
            if (arrmain[i] > max2)
            {
                max2 = arrmain[i];
            }
            printf(KRED "%i " RESET, arrmain[i]);
            y++;
        }
    }
    radixsort1(z, max1, newarr1);
    for (int i = 0; i < z; i++)
    {
        arrmain[index1[i]] = newarr1[i];
    }
    radixsort2(y, max2, newarr2);
    for (int i = 0; i < y; i++)
    {
        arrmain[index2[i]] = newarr2[i];
    }
    puts("");
    for (int i = 0; i < n; i++)
    {
        if (threetoten(arrmain[i]) > n / 2)
        {
            printf(KGRN "%i " RESET, arrmain[i]);
        }
        else
        {
            printf(KRED "%i " RESET, arrmain[i]);
        }
    }
    puts("");
    return 0;
}