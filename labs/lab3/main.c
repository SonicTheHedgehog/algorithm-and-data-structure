#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <stdbool.h>
#include <time.h>
#define KNRM "\x1B[0m"
#define KRED "\x1B[31m"
#define KGRN "\x1B[32m"
#define KYEL "\x1B[33m"
#define KBLU "\x1B[34m"
#define KMAG "\x1B[35m"
#define KCYN "\x1B[36m"
#define KWHT "\x1B[37m"
#define RESET "\x1B[0m"
int main()
{
    float a, b;
    printf("Enter the size of matrix:");
    scanf("%f%f", &a, &b);
    if (a - (int)a != 0 || a < 0 || b < 0 || b - (int)b != 0)
    {
        printf("Enter just natural and digit numbers\n");
    }
    else
    {
        int M = a;
        int N = b;
        int K = 0;
        printf("Enter the number to divide by: ");
        scanf("%i", &K);
        srand(time(0));
        int arr[M][N], arr1[100], mi[100], mj[100];
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                arr[i][j] = rand() % 101;
            }
        }
        puts("===========>Start matrix<============");
        int counter = 0;
        int n = 0;
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                n = arr[i][j] % K;
                if ((n == 0) || ((n % 2) != 0))
                {
                    arr1[counter] = arr[i][j];
                    mi[counter] = i;
                    mj[counter] = j;
                    counter++;
                    printf(KCYN "[%i] " RESET, arr[i][j]);
                }
                else
                {
                    printf("[%i] ", arr[i][j]);
                }
            }
            puts(" ");
        }
        puts("======================================");
        int sorted = 1;
        int gap = counter;
        while ((sorted == 1) && (gap > 1))
        {
            gap = (int)(gap / 1.3);
            sorted = 0;
            for (int i = 0; i < counter - gap; i++)
            {
                if (arr1[i] > arr1[i + gap])
                {
                    int z = arr1[i + gap];
                    arr1[i + gap] = arr1[i];
                    arr1[i] = z;
                    sorted = 1;
                }
            }
        }
        puts(" ");
        int h = 0;
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                n = arr[i][j] % K;
                if (n == 0 || n % 2 != 0)
                {
                    arr[i][j] = arr1[h];
                    mi[h] = i;
                    mj[h] = j;
                    h++;
                }
            }
        }
        puts(" ");
        puts("=============>End matrix<=============");
        h = 0;
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (i == mi[h] && j == mj[h])
                {
                    printf(KCYN "[%i] " RESET, arr[i][j]);
                    h++;
                }
                else
                    printf("[%i] ", arr[i][j]);
            }
            puts(" ");
        }
        puts("======================================");
    }
}