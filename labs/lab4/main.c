#include <stdio.h>
#include <stdlib.h>
struct SLNode
{
    int data;
    struct SLNode *next;
};
struct DLNode
{
    int data;
    struct DLNode *next;
    struct DLNode *prev;
};
struct DLNode *createDLNode(int data)
{
    struct DLNode *dnode = malloc(sizeof(struct SLNode));
    dnode->data = data;
    dnode->prev = NULL;
    dnode->next = NULL;
    return dnode;
}
struct SLNode *createSLNode(int data)
{
    struct SLNode *pnode = malloc(sizeof(struct SLNode));
    pnode->data = data;
    pnode->next = NULL;
    return pnode;
}
struct SLNode *slnode_push_front(struct SLNode *head, struct SLNode *node)
{
    node->next = head;
    head = node;
    return head;
}
struct DLNode *dlnode_push_front(struct DLNode *head, struct DLNode *node)
{
    node->next = head;
    node->prev = NULL;
    head = node;
    return head;
}
struct DLNode *dlnode_back(struct DLNode *head)
{
    if (head == NULL)
        return NULL;
    struct DLNode *tail = head;
    while (tail->next != NULL)
    {
        tail = tail->next;
    }
    return tail;
}
struct DLNode *dlnode_push_back(struct DLNode *head, struct DLNode *node)
{
    if (head == NULL)
    {
        return node;
    }
    else
    {
        struct DLNode *back = dlnode_back(head);
        back->next = node;
        node->prev = back;
        return head;
    }
}
void printSLList(struct SLNode *list)
{
    if (list == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        while (list != NULL)
        {
            printf("%i->", list->data);
            list = list->next;
        }
        puts(" ");
    }
}
void printDLList(struct DLNode *list)
{
    if (list == NULL)
    {
        printf("NULL\n");
    }
    else
    {
        while (list != NULL)
        {
            printf("%i-> ", list->data);
            list = list->next;
        }
        puts(" ");
    }
}
int sizeSL(struct SLNode *list)
{
    int size = 0;
    while (list != NULL)
    {
        list = list->next;
        size++;
    }
    printf("Deleted nodes= %i\n", size);
}
int sizeDL(struct DLNode *list)
{
    int size = 0;
    while (list != NULL)
    {
        list = list->next;
        size++;
    }
    printf("\nCurrent nodes= %i\n", size);
}

struct SLNode *createSecondList(struct DLNode *dlHead)
{
    struct DLNode *tmp = dlHead;
    struct SLNode *headSL = NULL;
    while (tmp != NULL)
    {
        if (tmp->data <= -9 || tmp->data >= 6)
        {
            headSL = slnode_push_front(headSL, createSLNode(tmp->data));
        }
        tmp = tmp->next;
    }
    return headSL;
}
struct DLNode *deleteDLNode(struct DLNode *node)
{
    struct DLNode *prev, *next;
    prev = node->prev;
    next = node->next;
    if (prev != NULL)
        prev->next = node->next;
    if (next != NULL)
        next->prev = node->prev;
    free(node);
    return (prev);
}
struct DLNode *getMinusNode(struct DLNode *head)
{
    struct DLNode *minus = NULL;
    if (head != NULL)
    {
        struct DLNode *node = head;
        while (node != NULL)
        {
            if (node->data < 0)
            {
                minus = node;
            }
            node = node->next;
        }
    }
    return minus;
}
struct DLNode *addDLNode(struct DLNode *head, struct DLNode *node)
{
    int num = head->data;
    struct DLNode *minus = getMinusNode(head);
    struct DLNode *temp = minus->next;
    if (temp != NULL)
    {
        node->next = temp;
        temp->prev = node;
    }
    minus->next = node;
    node->prev = minus;
    struct DLNode *buf = node;
    while (buf->data != num)
        buf = buf->prev;
    return buf;
}
struct DLNode *First(struct DLNode *head, int numb)
{
    int count = 0;
    struct DLNode *node = head;
    struct DLNode *newnode = createDLNode(numb);
    while (head->next != NULL)
    {
        if (head->data < 0)
        {
            count++;
        }
        head = head->next;
    }
    if (count == 0)
    {
        node = dlnode_push_back(node, newnode);
    }
    else
    {
        node = addDLNode(node, newnode);
    }
    sizeDL(node);
    printDLList(node);
    return node;
}
struct DLNode *Second(struct DLNode *head)
{
    struct SLNode *node = createSecondList(head);
    struct DLNode *tmp = head;
    sizeSL(node);
    printSLList(node);
    while (tmp != NULL)
    {
        if (tmp->data <= -9 || tmp->data >= 6)
            tmp->prev = deleteDLNode(tmp);
        tmp = tmp->next;
    }
    sizeDL(head);
    printDLList(head);
    return head;
}
int main()
{
    int n = 0;
    struct DLNode *head = NULL;
    while (n != 3)
    {
        int k = 0;
        printf("\nChoose operation:\n 1.Add new node;\n 2.Delete nodes;\n 3.Quit.\n");
        scanf("%i", &n);
        if (n == 1)
        {
            printf("Enter new node:");
            scanf("%i", &k);
            if (head == NULL)
            {
                head = dlnode_push_front(head, createDLNode(k));
                sizeDL(head);
                printDLList(head);
            }
            else
                head = First(head, k);
        }
        else if (n == 2)
        {
            if (head == NULL)
            {
                puts("Error");
            }
            else
                head = Second(head);
        }
    }
    exit;
}