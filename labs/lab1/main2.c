#include <stdio.h>
#include <math.h>
int main()
{
    long int n = 0, y = 0, x = 0, z = 0;
    printf("Enter the number: ");
    scanf("%li", &n);
    if (n <= 0)
        {printf("Error\n");}
    else if (n == 1)
        {printf("There is no palindrome\n");}
    else
        for (long int m = 1; m < n; m++)
        {
            x = pow(m, 2);
            y = x;
            z = 0;
            while (y > 0)
            {
                z += y % 10l;
                z *= 10l;
                y /= 10l;
            }
            z /= 10l;
            if (z == x)
            {
                printf("(%li)^2 = %li; \n", m, x);
            }
        }
    return (0);
}